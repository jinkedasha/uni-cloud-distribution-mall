export default {
	publicFun:{
		user: "user", //用户表
		revise_password: "revise_password", //修改用户登录密码
		address: "address", //用户地址表
		sign_in_user:"sign_in_user",//用户签到表
		wallet: "wallet", //用户钱包表
		banner:"banner", //轮播图表
		classification:"classification", //分类表
		activity:"activity", //活动表
		product:"product", //商品表
		comment:"comment", //商品评论表
		collection:"collection", //商品收藏表
		obtain_system:"obtain_system", //获取系统配置列表
		cart:"cart", //购物车表
		order:"order", //订单表
	},
	async uniCloudApi(name,data){
		let userinfo = uni.getStorageSync('userInfos');
		if(userinfo == ''){
			uni.showLoading({})
		}
		let res = await uniCloud.callFunction({
			name:name,
			data:data
		})
		if(!res.success){
			uni.showToast({
				icon:'none',
				title:'500000'
			})
			uni.hideLoading()
			return
		}
		if(res.result.data){
			uni.hideLoading()
			return res.result.data
		}else {
			uni.hideLoading()
			return res.result
		}
	},
	async uniCloudUpImage(url){
		let fileName = (new Date()).getTime();
		const result = await uniCloud.uploadFile({
			filePath: url,
			cloudPath: `${fileName}.jpg`
		});
		if(result.fileID){
			return result.fileID
		}
	}
}