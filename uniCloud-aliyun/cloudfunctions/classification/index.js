'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	if(event.class_type == 0){
	//获取全部分类
		let res = await obtain_class();
		return res
	}
};

//获取全部的分类
const obtain_class = async (event) => {
	const classification = db.collection('classification');
	let res = await classification.where({
		offNo: true
	}).get();
	return res
}