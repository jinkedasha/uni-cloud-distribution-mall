'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	const system = db.collection('system');
	if(event.system_name){
		let res = await system.where({system_name:event.system_name}).get();
		return res
	}else {
		let res = await system.get();
		return res
	}
};
