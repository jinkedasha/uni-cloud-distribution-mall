'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	if(event.comment_type == 0){
		let res = await obtain_comment(event);
		return res
	}
};

//获取指定商品的评论列表
const obtain_comment = async (event) => {
	const comment = db.collection('comment');
	let res = await comment.where({comment_product_id:event.product_id}).orderBy('comment_time','desc').skip(event.pageNum*event.pageSize).limit(event.pageSize).get();
	let count =  await comment.where({comment_product_id:event.product_id}).count();
	return {
		comment_list: res,
		comment_count: count
	}
}
