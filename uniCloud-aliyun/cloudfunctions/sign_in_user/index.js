'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	if (event.type == 0) {
	//type = 0;查询现在是否已经签到了
		let res = await now_sign(event);
		return res
	}else if(event.type == 1){
	//type = 1;开始签到
		let res = await begin_sign(event)
		return res
	}else if(event.type == 2){
	//type = 2;查询签到记录
		let res = await skip_sign_user(event)
		return res
	}
};
//查询现在是否已经签到了
const now_sign = async (event) => {
	const sign = db.collection('sign_in_user');
	let res = await sign.where({
		user_id: event._id,
		type: 0
	}).get();
	if (res.data.length > 0) {
		res = res.data[res.data.length - 1];
		const time = new Date(new Date().getTime() + 28800000);
		let ty = time.getFullYear();
		let tm = time.getMonth();
		let td = time.getDate();
		let tstr = `${ty}-${tm}-${td}`;
		let data = new Date(res.sign_time)
		let dy = data.getFullYear();
		let dm = data.getMonth();
		let dd = data.getDate();
		let dstr = `${dy}-${dm}-${dd}`;
		if (tstr === dstr) {
			return {
				code: 200,
				msg: true,
				time: res.sign_time
			}
		} else {
			return {
				code: 200,
				msg: false
			}
		}
	} else {
		return {
			code: 200,
			msg: false
		}
	}
}

//开始签到
const begin_sign = async(event) => {
	const sign = db.collection('sign_in_user');
	const user = db.collection('user');
	const time = new Date(new Date().getTime() + 28800000);
	let res = await sign.where({
		user_id: event._id,
		type: 0
	}).get();
	if (res.data.length > 0) {
		res = res.data[res.data.length - 1];
		let ty = time.getFullYear();
		let tm = time.getMonth();
		let td = time.getDate();
		let tstr = `${ty}-${tm}-${td}`;
		let data = new Date(res.sign_time)
		let dy = data.getFullYear();
		let dm = data.getMonth();
		let dd = data.getDate();
		let dstr = `${dy}-${dm}-${dd}`;
		if (tstr === dstr) {
			return {
				code: 200,
				msg: true,
				time: res.sign_time
			}
		} else {
			let result = await sign.add({
				user_id: event._id,
				integral_number: 2,
				sign_time: time.getTime() - 28800000,
				type: 0,
				descration: '签到获得积分'
			});
			let list = await user.where({
				_id: event._id
			}).get();
			let num = list.data[0].integral + 2; //获取到现在用户的积分并且加两点积分
			let nums = list.data[0].signIntegral + 2; //获取到用户截止到现在的总积分并加2点积分
			let numes = list.data[0].signInDays + 1; //获取到用户截止到如今的签到天数
			let up = await user.where({
				_id: event._id
			}).update({
				integral: num,
				signIntegral: nums,
				signInDays: numes
			});
			return up
		}
	} else {
		let result = await sign.add({
			user_id: event._id,
			integral_number: 2,
			sign_time: time.getTime(),
			type: 0,
			descration: '签到获得积分'
		});
		let list = await user.where({
			_id: event._id
		}).get();
		let num = list.data[0].integral + 2;
		let nums = list.data[0].signIntegral + 2;
		let numes = list.data[0].signInDays + 1;
		let up = await user.where({
			_id: event._id
		}).update({
			integral: num,
			signIntegral: nums,
			signInDays: numes
		});
		return up
	}
}

const skip_sign_user = async (event) => {
	const sign = db.collection('sign_in_user');
	let res = await sign.where({user_id:event._id}).orderBy('sign_time','desc').skip(event.pageNum*event.pageSize).limit(event.pageSize).get();
	return res
}