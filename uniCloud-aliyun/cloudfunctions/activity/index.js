'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	if(event.activity_type == 0){
	//获取全部可用的活动
		let res = await obtain_activity(event);
		return res
	}else if(event.activity_type == 1){
	//获取指定的活动
		let res = await obtain_activity_detail(event)
		return res
	}
};

//获取全部可用的活动
const obtain_activity = async (event) => {
	const activity = db.collection('activity');
	let res = await activity.where({acvicity_open:true}).get();
	return res
}

//获取指定的活动
const obtain_activity_detail = async (event) => {
	const activity = db.collection('activity');
	let res = await activity.where({_id:event.id}).get();
	return res
}
