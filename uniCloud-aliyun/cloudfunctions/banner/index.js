'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	if(event.banner_type == 0){
	//banner_type == 0获取正确的轮播图
		delete event.banner_type
		let res = await obtain_banner(event);
		return res
	}
};

//获取正确的轮播图
const obtain_banner =async (event) => {
	const banner = db.collection('banner');
	let data = new Date().getTime();
	const dbCmd = db.command;
	let res = await banner.where({
		start_time: dbCmd.lte(data),
		end_time: dbCmd.gt(data),
		type: event.type
	}).get()
	return res
}