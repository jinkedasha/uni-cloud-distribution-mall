'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	const or = db.collection('order');
	let or_all =await or.where({status:0}).get();
	or_all = or_all.data;
	let arr = [];
	for(let i in or_all){
		let n_now = new Date().getTime();
		if(n_now >=or_all[i].end_time){
			arr.push({
				status: await or.where({_id: or_all[i]._id}).update({status: 99}),
				id: or_all[i]._id,
				name: 'order'
			})
		}
	}
	
	const code_list = db.collection('code_list');
	let code_s = await code_list.where({
		status: 0
	}).get();
	code_s = code_s.data;
	for(let j in code_s){
		let now_time = new Date().getTime();
		if(code_s[j].create_time + 180000 >= now_time){
			arr.push({
				status: await code_list.where({_id: code_s[j]._id}).update({status: 1});
				id: code_s[j]._id,
				name: 'code_list'
			})
		}
	}
	
	return arr
};
