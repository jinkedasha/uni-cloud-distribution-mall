'use strict';
const db = uniCloud.database();
exports.main = async (event, context) => {
	if(event.wallet_type == 0){
	// wallet_type = 0添加用户提现钱包
		delete event.wallet_type
		let res =await add_wallet_user(event);
		return res
	}else if(event.wallet_type == 1){
	// wallet_type = 1获取用户提现钱包
		delete event.wallet_type
		let res =await obtain_wallet(event);
		return res
	}else if(event.wallet_type == 2){
	// wallet_type = 2删除用户提现钱包
		delete event.wallet_type
		let res =await remove_wallet_user(event);
		return res
	}
};

//添加用户提现钱包
const add_wallet_user = async (event) => {
	const wallet = db.collection('wallet_user');
	let res = await wallet.where({account:event.account}).count();
	if(res.total > 0){
		return {code:200,msg:"已存在此账户信息"}
	}else {
		let add = await wallet.add(event);
		return add
	}
}

//获取用户提现钱包

const obtain_wallet = async (event) => {
	const wallet = db.collection('wallet_user');
	let res = await wallet.where({user_id: event.user_id}).get();
	return res
}

//删除用户提现钱包
const remove_wallet_user = async (event) => {
	const wallet = db.collection('wallet_user');
	let res = await wallet.where({_id: event._id}).count();
	if(res.total == 1){
		res = await wallet.where({_id: event._id}).remove();
		return res
	}else {
		return {code: 200, msg:'此账号已被删除'}
	}
}